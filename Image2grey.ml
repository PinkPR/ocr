let makeMatrix (w,h,img) = 
  let matrix = Array.make_matrix w h (0,0,0) in
  for i = 0 to w-1 do
    for j = 0 to h-1 do
      matrix.(i).(j) <- Sdlvideo.get_pixel_color !img i j
    done;
  done;
  (matrix,w,h)

let level (matrix,w,h) = 
  let (r,g,b) = matrix.(w).(h) in
  (0.3 *. (float_of_int r) +.
  0.59 *. (float_of_int g) +.
  0.11 *. (float_of_int b)) /. 100.

let pixel2grey (matrix,w,h) =
  let e = int_of_float ((level (matrix,w,h)) *. 100.) in
    matrix.(w).(h) <- (e,e,e)

let image2grey (matrix,w,h) =
    for i=0 to w-1 do
      for j=0 to h-1 do
        pixel2grey (matrix,i,j)
      done;
    done;
    print_string("Image converted to grey\n");
    (matrix,w,h)
