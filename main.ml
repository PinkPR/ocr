let convert (matrix,w,h) =
let mat = Array.make_matrix w h false in
for i = 0 to w-1 do
  for j = 0 to h-1 do 
    if matrix.(i).(j) = (0,0,0) then mat.(i).(j) <- true
  done;
done;
(mat,w,h)

let run_args matrix = match Sys.argv.(1) with
|"-rot"       -> Binarize3.make_matrix (Filter.make_matrix matrix)
|"-filt1bin1" -> Binarize.make_matrix (Filter.make_matrix matrix)
|"-filt1bin2" -> Binarize2.make_matrix (Filter.make_matrix matrix)
|"-filt2bin1" -> Binarize.make_matrix (Filter2.make_matrix matrix)
|"-filt2bin2" -> Binarize2.make_matrix (Filter2.make_matrix matrix)
|"-filt1"     -> Filter.make_matrix matrix
|"-filt2"     -> Filter2.make_matrix matrix
|"-filt1bin3" -> Binarize3.make_matrix (Filter.make_matrix matrix)
|"-filt2bin3" -> Binarize3.make_matrix (Filter2.make_matrix matrix) 
|_            -> Binarize.make_matrix (Filter.make_matrix matrix)

let _ = 
  let matrixtrip = Image2grey.makeMatrix (Img_Load.getimg Sys.argv.(2)) in
  let matrix = Image2grey.image2grey matrixtrip in
  Img_Load.saveimg_from_matrix (Sys.argv.(2)) ("image_processed.bmp") (Rotate.rotate_matrix (convert (run_args matrix)))

