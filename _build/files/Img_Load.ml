let getimg path =
  Sdl.init(); 
  let img = Sdlloader.load_image path in
  let (w,h,i) = ((Sdlvideo.surface_info img).Sdlvideo.w,
                 (Sdlvideo.surface_info img).Sdlvideo.h,
                 img) in
  (w,h, ref i)
  
let saveimg img name =
  SDlvideo.save_BMP img name;
  print_string("Process succeed ; image saved\n\n");
  Sdl.quit()

let saveimg_from_matrix path name (matrix,w,h) = 
  let img = Sdlloader.load_image path in
  for i = 0 to w-1 do
    for j = 0 to h-1 do
      Sdlvideo.put_pixel_color img i j (matrix.(i).(j))
    done;
  done;
  saveimg img name

(* let saveimg img name = 
  SDlvideo.save_BMP img name;
  print_string("Process succeed ; image saved\n\n");
  Sdl.quit(); *)
