let convert_matrix (matrix,w,h) =
  let matrix2 = Array.make_matrix w h 0 in
  for i = 0 to w-1 do
    for j = 0 to h-1 do
      matrix2.(i).(j) <- match matrix.(i).(j) with
                         |(a,b,c) -> a
    done;
  done;
  print_string("Filter : Matrix converted\n");
  (matrix2,w,h)

let get_filter_array x y (matrix,w,h) =
  let a = Array.make (3*3) 0 in
  for count = 0 to 8 do
  for i = x-1 to x+1 do
    for j = y-1 to y+1 do
      Array.set a count matrix.(i).(j);
    done;
  done;
  done;
  Array.fast_sort (-) a;
  a

let get_median_color x y (matrix,w,h) =
  let a = get_filter_array x y (matrix,w,h) in
  let c = Array.get a 3 in
  (c,c,c) 

let make_matrix (matrix,w,h) =
  let matrix2 = convert_matrix (matrix,w,h) in
  (* let matrix3 = matrix in *)
  for i = 1 to w-2 do
    for j = 1 to h-2 do
      matrix.(i).(j) <- get_median_color i j matrix2
    done;
  done;
  print_string("Filter : matrix filtered\n");
  (matrix,w,h)  

 
