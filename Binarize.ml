let get_local_treshhold x y (matrix,w,h) =
  let min = ref 0 in
  let max = ref 0 in
  for i = x-1 to x+1 do
    for j = y-1 to y+1 do
      let e = match matrix.(i).(j) with
              |(a,b,c) -> a in
      if (e > !max) then
        max := e;
      if (e < !min) then
        min := e
    done;
  done;
  (!max + !min)/2


let make_matrix (matrix,w,h) =
  let matrix2 = matrix in
  for i = 1 to w-2 do
    for j = 1 to h-2 do
      let c = match matrix.(i).(j) with
              |(a,b,c) -> a in
      let e = get_local_treshhold i j (matrix,w,h) in
      if (c <= e) then matrix2.(i).(j) <- (0,0,0)
      else matrix2.(i).(j) <- (255,255,255)
    done;
  done;
  print_string("Binarize : matrix binarized\n");
  (matrix2,w,h) 
