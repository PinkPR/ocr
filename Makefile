OCAML=ocamlopt
OCAMLFLAGS= -I +sdl
OCAMLLD= bigarray.cmxa sdl.cmxa sdlloader.cmxa

ocr: Matrix.ml RotateSerial.ml Rotate.ml Binarize3.ml Binarize2.ml Binarize.ml Filter2.ml Filter.ml Image2grey.ml Img_Load.ml main.ml
	${OCAML} ${OCAMLFLAGS} ${OCAMLLD} -o ocr Matrix.ml RotateSerial.ml Rotate.ml Binarize3.ml Binarize2.ml Binarize.ml Filter2.ml Filter.ml Image2grey.ml Img_Load.ml main.ml

clean:
	rm -f *~ *.o *.cm? ocr
