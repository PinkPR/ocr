let get_treshhold (matrix,w,h) =
  let e = ref 0 in
  for i = 0 to w-1 do
    for j = 0 to h-1 do
      e := !e + match matrix.(i).(j) with
                |(a,b,c) -> a
    done;
  done;
  print_string("Binarize2 : treshhold obtained\n");
  !e / (w*h)

let make_matrix (matrix,w,h) =
  let e = get_treshhold (matrix,w,h) in
  for i = 0 to w-1 do
    for j = 0 to h-1 do
      let g = match matrix.(i).(j) with
              |(a,b,c) -> a in
      if (g <= e) then matrix.(i).(j) <- (0,0,0)
      else matrix.(i).(j) <- (255,255,255)
    done;
  done;
  print_string("Binarize2 : matrix binarized\n");
  (matrix,w,h)  
