let get_middle_color x y (matrix,w,h) =
  let color = ref 0 in
  for i = x-1 to x+1 do
    for j = y-1 to y+1 do
      color := !color + match matrix.(i).(j) with
                        |(a,b,c) -> a
    done;
  done;
  !color / 9

let make_matrix (matrix,w,h) =
  let matrix2 = matrix in
  for i = 1 to w-2 do
    for j = 1 to h-2 do
      let c = get_middle_color i j (matrix,w,h) in
      matrix2.(i).(j) <- (c,c,c)
    done;
  done;
  print_string("Filter2 : matrix filtered\n");
  (matrix2,w,h) 
