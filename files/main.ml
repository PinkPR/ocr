let run_args matrix = match Sys.argv.(1) with
|"-filt1bin1" -> Binarize.make_matrix (Filter.make_matrix matrix)
|"-filt1bin2" -> Binarize2.make_matrix (Filter.make_matrix matrix)
|"-filt2bin1" -> Binarize.make_matrix (Filter2.make_matrix matrix)
|"-filt2bin2" -> Binarize2.make_matrix (Filter2.make_matrix matrix)
|"-filt1"     -> Filter.make_matrix matrix
|"-filt2"     -> Filter2.make_matrix matrix
|"-filt1bin3" -> Binarize3.make_matrix (Filter.make_matrix matrix)
|"-filt2bin3" -> Binarize3.make_matrix (Filter2.make_matrix matrix) 
|_            -> Binarize.make_matrix (Filter.make_matrix matrix)

let _ = 
  let matrixtrip = Image2grey.makeMatrix (Img_Load.getimg Sys.argv.(2)) in
  let matrix = Image2grey.image2grey matrixtrip in
  Img_Load.saveimg_from_matrix (Sys.argv.(2)) ("image_filtered.bmp") (run_args matrix)

