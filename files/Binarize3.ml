let make_matrix (matrix,w,h) =
  for i = 0 to w-1 do
    for j = 0 to h-1 do
      match matrix.(i).(j) with
      |(a,_,_) -> if a < 180 then 
                    matrix.(i).(j) <- (0,0,0)
                  else
                    matrix.(i).(j) <- (255,255,255)  
    done;
  done;
  print_string("Binarize : matrix binarized\n");
  (matrix,w,h) 
