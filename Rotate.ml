let convert matrix =
let (w,h) = Matrix.get_dims matrix in
let mat = Array.make_matrix w h (255,255,255) in
for i = 0 to w-1 do
  for j = 0 to h-1 do
    if matrix.(i).(j) then mat.(i).(j) <- (0,0,0)
  done;
done;
mat

let rotate_matrix (matrix,w,h) = 
  let mat = convert (RotateSerial.rotate matrix (RotateSerial.get_skew_angle matrix)) in 
  (mat,w,h)
